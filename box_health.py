#!/usr/bin/env python3

# Copyright 2018 Jacques Supcik / HEIA-FR
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This program publish box's health info them to redis.
#

import json
import logging
import math
import random
import time

import click
import psutil
import redis

try:
    import Adafruit_DHT
    sensor = Adafruit_DHT.DHT22
    pin = 4
    sim = False
except ImportError:
    sim = True

logger = logging.getLogger(__name__)


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--redis', 'redis_url', default="redis://")
def main(debug, redis_url):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    r = redis.Redis.from_url(redis_url)

    now = time.time()
    next_run = math.ceil(now)

    while True:
        dt = next_run - now
        if dt > 0:
            time.sleep(dt)
        if sim:
            res = {
                't': 15 + random.random() * 50,
                'h': 900 + random.random() * 300,
            }
        else:
            h, t = Adafruit_DHT.read_retry(sensor, pin)
            if h is not None:
                h = round(h, 2)
            if t is not None:
                t = round(t, 2)
            res = {
                "t": t,
                "h": h,
            }

        res['msgType'] = 'from-box'
        res['cpuload'] = psutil.cpu_percent()
        res['vm'] = psutil.virtual_memory()
        res['disk'] = psutil.disk_usage('/')
        try:
            res['sensors'] = psutil.sensors_temperatures()
        except AttributeError:
            pass

        logger.debug(json.dumps(res))
        r.publish("heiafr:hydrocontest:message", json.dumps(res))

        now = time.time()
        next_run += 5


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    main()
