# Hydrocontest - Box Health

Box health provides environment information from the ground station. Currently, it provides the temperature and the humidity of the box.